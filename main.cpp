#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <sstream>

using namespace std;

int solve(int *from, int *to, int size);

void printDirections(const map<int, set<int> *> *directions);

void calculateTres(const int *from, const int *to, int size, int rootIndex, map<int, set<int> *> &tree);

int calculateScoreForRoot(map<int, set<int> *> &tree, map<int, set<int> *> &directions,
                          int elem);

string setToString(const set<int> *arg);

int
findBestScore(map<int, set<int> *> *directions, int startingRootElem, map<int, set<int> *> &tree, int scoreForRoot);

int main() {

    int size = 6;
    int from[] = {7, 2, 3, 1, 3, 1};
    int to[] = {2, 6, 4, 2, 1, 5};

/*    int size = 4;
    int from[] = {2, 3, 4, 5};
    int to[] = {1, 1, 1, 1};*/

/*    int size = 4;
    int from[] = {1, 1, 1, 1};
    int to[] = {2, 3, 4, 5};*/

    int result = solve(from, to, size);

    cout << "Result: " << result;

    return 0;
}


vector<int> findConnectedElements(const int *from, const int *to, int size, int elem) {

    vector<int> nextElements;
    for (int i = 0; i < size; i++) {
        if (from[i] == elem) {
            nextElements.push_back(to[i]);
        }
    }

    for (int i = 0; i < size; i++) {
        if (to[i] == elem) {
            nextElements.push_back(from[i]);
        }
    }

    return nextElements;
}

void addToTree(map<int, set<int> *> *tree, int key, int value) {
    set<int> *&values = (*tree)[key];
    if (values == NULL) {
        values = new set<int>;
    }
    values->insert(value);
    cout << "Added " << key << " -> " << value << endl;
}

map<int, set<int> *> *mapDirections(int *from, int *to, int size) {
    map<int, set<int> *> *result = new map<int, set<int> *>();
    for (int i = 0; i < size; i++) {
        int currFrom = from[i];
        int currTo = to[i];
        set<int> *&values = (*result)[currFrom];
        if (values == NULL) {
            values = new set<int>;
        }
        values->insert(currTo);
    }
    return result;
};

struct ElemScore {
    int elem;
    int score;
};

int solve(int *from, int *to, int size) {

    map<int, set<int> *> *directions = mapDirections(from, to, size);
    //directions to mapa kierunku polaczen (kluczami sa bazy a wartosciami jest set baz do ktorych jest polaczenie)

    int startingRootIndex = 0;
    int startingRootElem = from[startingRootIndex];

    map<int, set<int> *> tree;
    //drzewo polaczen od korzenia bez wzgledu na "zwrot" polaczen baz

    calculateTres(from, to, size, startingRootIndex, tree);

    printDirections(directions);

    int scoreForRoot = calculateScoreForRoot(tree, *directions, startingRootElem);

    int bestScore = findBestScore(directions, startingRootElem, tree, scoreForRoot);

    return bestScore;
}

int
findBestScore(map<int, set<int> *> *directions, int startingRootElem, map<int, set<int> *> &tree, int scoreForRoot) {
    vector<ElemScore *> acc;
    ElemScore *first = new ElemScore();
    first->elem = startingRootElem;
    first->score = scoreForRoot;
    acc.push_back(first);

    int smallestScore = first->score;

    while (!acc.empty()) {
        ElemScore *current = acc.back();
        acc.pop_back();

        const map<int, set<int> *>::iterator &maybeNextElem = tree.find(current->elem);
        if (maybeNextElem != tree.end()) {
            set<int> *&nextElems = (*maybeNextElem).second;
            for (set<int>::iterator it = nextElems->begin(); it != nextElems->end(); it++) {
                int nextElem = *it;
                int scoreDifference = 0;

                const map<int, std::set<int> *>::iterator &currDirIter = directions->find(current->elem);
                if (currDirIter != directions->end()) {
                    set<int> *&at = (*currDirIter).second;
                    if (at->find(nextElem) != at->end()) {
                        scoreDifference = 1;
                    } else {
                        scoreDifference = -1;
                    }
                    int nextElemScore = current->score + scoreDifference;
                    cout << "score for " << nextElem << " is " << nextElemScore << endl;
                    if (nextElemScore < smallestScore) {
                        cout << "setting lowest score for elem " << nextElem << " with score " << nextElemScore << endl;
                        smallestScore = nextElemScore;
                    }

                    ElemScore *next = new ElemScore();
                    next->elem = nextElem;
                    next->score = nextElemScore;
                    acc.push_back(next);
                }
            }
        }
    }
    return smallestScore;
}

int calculateScoreForRoot(map<int, set<int> *> &tree, map<int, set<int> *> &directions,
                          int elem) {

    int score = 0;
    vector<int> acc;
    acc.push_back(elem);

    while (!acc.empty()) {
        int currentElem = acc.back();
        acc.pop_back();

        const map<int, std::set<int> *>::iterator &maybeNextElem = tree.find(currentElem);
        if (maybeNextElem != tree.end()) {
            set<int> *&nextElems = (*maybeNextElem).second;

            cout << "set for " << currentElem << " is " << setToString(nextElems) << endl;
            for (set<int>::iterator it = nextElems->begin(); it != nextElems->end(); it++) {
                int nextElem = *it;
                acc.push_back(nextElem);
                const map<int, std::set<int> *>::iterator &iterator = directions.find(currentElem);
                if (iterator != directions.end()) {
                    set<int> *elemDirectionElems = (*iterator).second;
                    if (elemDirectionElems->find(nextElem) == elemDirectionElems->end()) {
                        score++;
                        cout << " -1 for " << currentElem << " !-> " << nextElem << endl;
                    }
                } else {
                    score++;
                    cout << " -1 for " << currentElem << " !-> " << nextElem << endl;
                }
            }
        }
    }

    cout << "root score for " << elem << " is " << score << endl;
    return score;
}

void calculateTres(const int *from, const int *to, int size, int rootIndex, map<int, set<int> *> &tree) {
    vector<int> acc = findConnectedElements(from, to, size, from[rootIndex]);
    for (int i = 0; i < acc.size(); i++) {
        addToTree(&tree, from[rootIndex], acc[i]);
    }

    while (!acc.empty()) {
        int currentElem = acc.back();
        acc.pop_back();
        const vector<int> &nextElements = findConnectedElements(from, to, size, currentElem);

        for (int i = 0; i < nextElements.size(); i++) {
            int nextElem = nextElements[i];
            if (tree.find(nextElem) == tree.end()) {//if key not found
                addToTree(&tree, currentElem, nextElem);
                acc.push_back(nextElem);
            }
        }
    }
}

void printDirections(const map<int, set<int> *> *directions) {

    map<int, std::set<int> *>::const_iterator it;
    for (it = directions->begin(); it != directions->end(); it++) {
        std::cout << it->first << " ->  " << setToString(it->second) << std::endl;
    }

}

string setToString(const set<int> *arg) {
    std::ostringstream result;

    set<int>::iterator it;
    for (it = arg->begin(); it != arg->end(); it++) {
        int value = *it;
        result << value;
        result << ", ";
    }

    string res = result.str();
    return res.substr(0, res.size());
}



